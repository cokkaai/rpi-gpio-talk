# Contents

A brief introducion to physical computing and the simplest "light on the LED" example.
The example uses the (GPIO zero library)[https://gpiozero.readthedocs.io].
Plenty on links to further explore physical computing.

# How it is made

The presentation is build with Reveal.js and vim.
